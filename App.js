import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { capitalizeFirstLetter } from './src/util';
import PeoplePage from './src/pages/PeoplePage';
import PeopleDetailPage from './src/pages/PeopleDetailPage';

const AppNavigator = createStackNavigator({
  'Main': {
    screen: PeoplePage
  },
  'PeopleDetail': {
    screen: PeopleDetailPage,
    navigationOptions: ({ navigation }) => {
      const peopleName = capitalizeFirstLetter(navigation.state.params.people.name.first);
      return ({
        title: peopleName ,
        headerTitleStyle:{
          color: 'white',
          backgroundColor: '#6ca2f7',
          fontSize: 30
        }
      });
      
    } 
  }

},{
  defaultNavigationOptions:{
    title: 'Pessoa!',
    headerTintColor: '#FFF',
    headerStyle:{
      backgroundColor: '#6ca2f7',
      borderBottomWidth: 1,
      borderBottomColor: '#C5C5C5'

    },
    headerTitleStyle:{
      color: '#FFF',
      fontSize: 30,
      flexGrow: 1,
      textAlign: 'center'
    }
  }
});

const AppContainer = createAppContainer(AppNavigator); 
export default AppContainer;
