import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native';
import PeopleList from '../components/PeopleList';
import axios from 'axios';


export default class PeoplePage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      peoples: [],
      loading: false,
      error: false,
    };
  }

  componentDidMount(){
    this.setState({ loading: true });
    axios.get('https://randomuser.me/api/?nat=br&results=50')
      .then(response => {
      const { results } = response.data;
      //const names = results.map(people => people.name.first);
      //console.log(names);
      //console.log(response.data.results);
      this.setState({
        peoples: results,
        loading: false,
      });
    }).catch(error => {
        this.setState({ 
          loading: false,
          error:true
        })
    });
  }

  render(){
    
    return (
      <View style={styles.container}>
        { 
          this.state.loading 
          ? <ActivityIndicator size="large" color="#000" /> 
          : this.state.error ? <Text style={styles.error}>Ops.. algo deu errado =( </Text> 
            : <PeopleList 
          peoples={this.state.peoples}
          onPressItem={pageParams =>{
            this.props.navigation.navigate('PeopleDetail', pageParams);
          }} />
        }
        
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  error: {
    color: 'red',
    alignSelf: 'center'
  }
});